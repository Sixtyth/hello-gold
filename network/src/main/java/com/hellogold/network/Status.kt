package com.hellogold.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}