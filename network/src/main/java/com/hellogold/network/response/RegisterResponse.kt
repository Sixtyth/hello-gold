package com.hellogold.network.response

import com.google.gson.annotations.SerializedName

data class RegisterResponse (@SerializedName("api_token") val apiToken: String,
                             @SerializedName("public_key") val publicKey: String,
                             @SerializedName("api_key") val apiKey: String,
                             @SerializedName("account_number") val accountNumber: String)