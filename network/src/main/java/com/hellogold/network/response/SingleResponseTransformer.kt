package com.hellogold.network.response

import io.reactivex.Single
import io.reactivex.SingleTransformer

class SingleResponseTransformer<D> : SingleTransformer<ResponseEntity<D>, D> {
    override fun apply(upstream: Single<ResponseEntity<D>>): Single<D> {
        return upstream.map { (status, statusCode, data, message) ->
            if (status == ResponseStatus.ERROR || data == null) {
                throw APIException(message.toString(), statusCode, message?.get("failure_code"))
            }
            data
        }
    }
}