package com.hellogold.network.response

import com.google.gson.annotations.SerializedName

enum class ResponseStatus {
    @SerializedName("ok") SUCCESS,
    @SerializedName("error") ERROR
}