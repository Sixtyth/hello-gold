package com.hellogold.network.response

import com.google.gson.annotations.SerializedName

data class ResponseEntity<D>(@SerializedName("status") val status: ResponseStatus,
                             @SerializedName("status_code") val statusCode: Int?,
                             @SerializedName("data") val data: D?,
                             @SerializedName("message") val message: ErrorMessage?)