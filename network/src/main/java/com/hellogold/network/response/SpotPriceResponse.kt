package com.hellogold.network.response

import com.google.gson.annotations.SerializedName

data class SpotPriceResponse (@SerializedName("buy") val buy: Double,
                      @SerializedName("sell") val sell: Double,
                      @SerializedName("spot_price") val spotPrice: Double?,
                      @SerializedName("timestamp") val timestamp: String)