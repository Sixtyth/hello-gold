package com.hellogold.network

import com.hellogold.network.response.RegisterResponse
import com.hellogold.network.response.ResponseEntity
import com.hellogold.network.response.SpotPriceResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface HelloGoldApi {

    @GET("v2/spot_price.json")
    fun getSpotPrice(): Single<ResponseEntity<SpotPriceResponse>>

    @POST("v3/users/register.json")
    fun register(
        @Query("email") email: String,
        @Query("uuid") uuid: String,
        @Query("data") data: String,
        @Query("tnc") tnc: String,
        @Query("user") user: String
    ): Single<ResponseEntity<RegisterResponse>>
}