package com.hellogold.network.repository

import com.hellogold.network.HelloGoldApi
import com.hellogold.network.response.RegisterResponse
import com.hellogold.network.response.SingleResponseTransformer
import com.hellogold.network.response.SpotPriceResponse
import io.reactivex.Single

open class HelloGoldRepository(private val helloGoApi: HelloGoldApi) {

    fun getGoldPrice(): Single<SpotPriceResponse> {
        return helloGoApi.getSpotPrice().compose(SingleResponseTransformer())
    }

    fun register(
        email: String,
        uuid: String,
        data: String,
        tnc: String,
        user: String
    ): Single<RegisterResponse> {
        return helloGoApi.register(email, uuid, data, tnc, user).compose(SingleResponseTransformer())
    }

}