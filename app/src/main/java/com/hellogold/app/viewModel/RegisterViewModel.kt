package com.hellogold.app.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hellogold.app.service.NetworkError
import com.hellogold.network.repository.HelloGoldRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RegisterViewModel(private val repo: HelloGoldRepository) : DisposableViewModel() {

    private val mutableStatus = MutableLiveData<Status>()

    val status: LiveData<Status> = mutableStatus

    private val currentValue: Status
        get() = mutableStatus.value!!

    init {
        mutableStatus.value = Status.Loading(ViewDataBundle())
    }

    fun register(
        email: String,
        uuid: String,
        data: String,
        tnc: String,
        user: String
    ) {
        addDisposable(
            repo.register(email, uuid, data, tnc, user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mutableStatus.value =
                        Status.Success(ViewDataBundle(it.apiToken, it.publicKey, it.apiKey, it.accountNumber))
                }, {
                    mutableStatus.value =
                        Status.Error(NetworkError(it), currentValue.viewDataBundle)
                })
        )

    }

    sealed class Status(val viewDataBundle: ViewDataBundle) {
        class Loading(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Success(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Error(val networkError: NetworkError, viewDataBundle: ViewDataBundle) :
            Status(viewDataBundle)
    }

    data class ViewDataBundle(
        val apiToken: String? = null,
        val publicKey: String? = null,
        val apiKey: String? = null,
        val accountNumber: String? = null
    )

}