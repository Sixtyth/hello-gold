package com.hellogold.app.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.hellogold.app.service.NetworkError
import com.hellogold.network.repository.HelloGoldRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GoldSportViewModel(private val repo: HelloGoldRepository) : DisposableViewModel() {

    private val mutableStatus = MutableLiveData<Status>()

    val goldStatus: LiveData<Status> = mutableStatus

    private val currentValue: Status
        get() = mutableStatus.value!!

    init {
        mutableStatus.value = Status.Loading(ViewDataBundle())
    }

    fun getGoldSpotPrice() {
        addDisposable(
            repo.getGoldPrice()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mutableStatus.value =
                        Status.Success(ViewDataBundle(it.spotPrice ?: 0.0, it.timestamp))
                }, {
                    mutableStatus.value =
                        Status.Error(NetworkError(it), currentValue.viewDataBundle)
                })
        )

    }


    sealed class Status(val viewDataBundle: ViewDataBundle) {
        class Loading(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Success(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Error(val networkError: NetworkError, viewDataBundle: ViewDataBundle) :
            Status(viewDataBundle)
    }

    data class ViewDataBundle(val spotPrice: Double? = 0.0, var timestamp: String? = null)
}