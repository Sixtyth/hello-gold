package com.hellogold.app.koin

import com.hellogold.app.persistence.Preferences
import com.hellogold.app.service.*
import com.hellogold.app.viewModel.GoldSportViewModel
import com.hellogold.app.viewModel.RegisterViewModel
import com.hellogold.network.repository.HelloGoldRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val prefModule = module {
    single { Preferences(androidContext()) }
}

val networkModule = module {
    factory { provideLoggingInterceptor() }
    single { provideRetrofit(get(), get()) }
    single { provideOkHttpClient() }
    single { provideGson() }
}

val viewModelModule = module {
    factory { GoldSportViewModel(get()) }
    factory { RegisterViewModel(get()) }
}

val forecastModule = module {
    factory { HelloGoldRepository(get()) }
}