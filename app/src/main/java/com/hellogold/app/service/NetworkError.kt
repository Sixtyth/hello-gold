package com.hellogold.app.service

import java.io.IOException

class NetworkError(cause: Throwable?) : Throwable(cause) {
    private val error: Throwable? = cause

    fun getAppErrorState(): ErrorState {
        if (this.error is IOException) return ErrorState.NO_CONNECTION
        return ErrorState.DEFAULT
    }

    enum class ErrorState {
        DEFAULT, NO_CONNECTION
    }
}