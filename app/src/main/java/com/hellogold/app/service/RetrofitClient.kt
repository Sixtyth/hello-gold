package com.hellogold.app.service

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.hellogold.app.BuildConfig
import com.hellogold.network.HelloGoldApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun provideOkHttpClient(): OkHttpClient {
    val builder = OkHttpClient
        .Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
    return builder.build()
}

fun provideRetrofit(httpOk :OkHttpClient, gson: Gson): HelloGoldApi {

    val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.HOST)
        .client(httpOk)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    return retrofit.create(HelloGoldApi::class.java)
}

fun provideGson() : Gson {
    return GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
}


fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BASIC
    return logger
}
