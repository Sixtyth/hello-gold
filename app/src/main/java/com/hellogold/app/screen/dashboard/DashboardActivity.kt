package com.hellogold.app.screen.dashboard

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.hellogold.app.R
import com.hellogold.app.service.NetworkError
import com.hellogold.app.utils.DateTimeUtil
import com.hellogold.app.viewModel.GoldSportViewModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardActivity : AppCompatActivity() {

    // Lazy injected Presenter instance
    private val viewModel: GoldSportViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        viewModel.getGoldSpotPrice()

        viewModel.goldStatus.observe(this, Observer { status ->

            when(status) {

                is GoldSportViewModel.Status.Loading -> {

                }

                is GoldSportViewModel.Status.Success -> {
                    dashboard_swipe_new_gold.isRefreshing = false
                    status.viewDataBundle.let {
                        Log.i("Success", status.viewDataBundle.spotPrice.toString())
                        dashboard_spot_price.text = getString(R.string.spot_price, it.spotPrice)
                        dashboard_time.text = DateTimeUtil.toDateTime(it.timestamp ?: "2019-11-10T11:28:03.794+00:00")
                    }

                }

                is GoldSportViewModel.Status.Error -> {
                    when (status.networkError.getAppErrorState()) {
                        NetworkError.ErrorState.DEFAULT -> {
                            Log.i("Error", "Error")
                        }
                        NetworkError.ErrorState.NO_CONNECTION -> {
                            Log.i("Error", "Error")
                        }
                    }
                }

            }

        })

        dashboard_swipe_new_gold.setOnRefreshListener {
            viewModel.getGoldSpotPrice()
        }

    }

}