package com.hellogold.app.screen.register

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.hellogold.app.R
import com.hellogold.app.persistence.Preferences
import com.hellogold.app.screen.dashboard.DashboardActivity
import com.hellogold.app.service.NetworkError
import com.hellogold.app.utils.GenerateUtil
import com.hellogold.app.viewModel.RegisterViewModel
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterActivity : AppCompatActivity() {

    // Lazy injected Presenter instance
    private val viewModel: RegisterViewModel by viewModel()
    private val store: Preferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        supportActionBar?.title = getString(R.string.title_register_activity)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (!store.getToken().isNullOrBlank()) {
            nextPage()
        }

        viewModel.status.observe(this, Observer { status ->

            when (status) {

                is RegisterViewModel.Status.Loading -> {

                }

                is RegisterViewModel.Status.Success -> {
                    status.viewDataBundle.let {
                        store.storeApiToken(it.apiToken ?: "")
                        store.storePublicKey(it.publicKey ?: "")
                        store.storeApiKey(it.apiKey ?: "")
                        store.storeAccountNumber(it.accountNumber ?: "")
                    }
                    nextPage()
                }

                is RegisterViewModel.Status.Error -> {
                    when (status.networkError.getAppErrorState()) {
                        NetworkError.ErrorState.DEFAULT -> {
                            Log.i("Error", "Error")
                        }
                        NetworkError.ErrorState.NO_CONNECTION -> {
                            Log.i("Error", "Error")
                        }
                    }
                }

            }

        })

        register_button.setOnClickListener {
            if (!validateEmail() || !validatePassword() || !validateTermsOfService()) {
                return@setOnClickListener
            } else {
                viewModel.register(register_text_input_email.editText?.text.toString().trim(),
                    GenerateUtil.uuid(),
                    GenerateUtil.randomString(8),
                    register_checkbox.isChecked.toString(),
                    register_text_input_email.editText?.text.toString().trim())
            }
        }

    }

    private fun validateEmail(): Boolean {
        val emailInput = register_text_input_email.editText?.text.toString().trim()

        return if (emailInput.isEmpty()) {
            register_text_input_email.error = "Field can't be empty"
            false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            register_text_input_email.error = "Please enter a valid email address"
            false
        } else {
            register_text_input_email.error = null
            true
        }
    }

    private fun validateTermsOfService(): Boolean {

        val isAccept = register_checkbox.isChecked

        Log.i("TermsOfService", isAccept.toString())

        return isAccept

    }

    private fun validatePassword(): Boolean {
        val passwordInput = register_text_input_password.editText?.text.toString().trim()

        return when {
            passwordInput.isEmpty() -> {
                register_text_input_password.error = "Field can't be empty"
                false
            }
            passwordInput.length < 8 -> {
                register_text_input_password.error = "Password must be a minimum of 8 characters."
                false
            }
            else -> {
                register_text_input_password.error = null
                true
            }
        }
    }

    private fun nextPage() {
        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_register, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.ask -> {
                Log.i("Click", "Whoaaaaa you click me")
                return true
            }
        }
        return super.onOptionsItemSelected(item!!)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
