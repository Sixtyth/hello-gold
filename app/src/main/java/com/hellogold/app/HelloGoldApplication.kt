package com.hellogold.app

import android.app.Application
import com.hellogold.app.koin.forecastModule
import com.hellogold.app.koin.networkModule
import com.hellogold.app.koin.prefModule
import com.hellogold.app.koin.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class HelloGoldApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@HelloGoldApplication)
            modules(listOf(prefModule, networkModule, viewModelModule, forecastModule))
        }
    }

}