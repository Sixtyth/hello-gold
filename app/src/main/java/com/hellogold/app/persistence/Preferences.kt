package com.hellogold.app.persistence

import android.content.Context
import android.content.SharedPreferences

class Preferences(context: Context) {

    private val preferences: SharedPreferences =
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)

    fun storeApiToken(token: String) {
        preferences.edit().putString(API_TOKEN, token).apply()
    }

    fun getToken(): String? {
        return preferences.getString(API_TOKEN, null)
    }

    fun storePublicKey(key: String) {
        preferences.edit().putString(PUBLIC_KEY, key).apply()
    }

    fun getPublicKey(): String? {
        return preferences.getString(PUBLIC_KEY, null)
    }

    fun storeApiKey(key: String) {
        preferences.edit().putString(API_KEY, key).apply()
    }

    fun getApiKey(): String? {
        return preferences.getString(API_KEY, null)
    }

    fun storeAccountNumber(account: String) {
        preferences.edit().putString(ACCOUNT_NUMBER, account).apply()
    }

    fun getAccountNumber(): String? {
        return preferences.getString(ACCOUNT_NUMBER, null)
    }

    companion object {
        private const val API_TOKEN = "API_TOKEN"
        private const val PUBLIC_KEY = "PUBLIC_KEY"
        private const val API_KEY = "API_KEY"
        private const val ACCOUNT_NUMBER = "ACCOUNT_NUMBER"
    }

}