package com.hellogold.app.utils

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {

    fun toDateTime(string: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val output = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        val d = sdf.parse(string)
        return output.format(d!!)
    }

}