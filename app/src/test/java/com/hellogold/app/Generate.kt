package com.hellogold.app

import junit.framework.TestCase.assertNotNull
import org.junit.Test
import java.security.SecureRandom
import java.util.UUID.randomUUID

class Generate {

    @Test
    fun generateRandomString() {

        val length = 8
        val CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz"
        val CHAR_UPPER = CHAR_LOWER.toUpperCase()
        val NUMBER = "0123456789"

        val DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER
        val random = SecureRandom()

        require(length >= 1)

        val sb = StringBuilder(length)
        for (i in 0 until length) {

            // 0-62 (exclusive), random returns 0-61
            val rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length)
            val rndChar = DATA_FOR_RANDOM_STRING[rndCharAt]

            // debug
            /*System.out.format("%d\t:\t%c%n", rndCharAt, rndChar)*/

            sb.append(rndChar)

        }

        println(sb.toString())
        assertNotNull(sb.toString())

    }

    @Test
    fun uuid() {
        //initialize uuid
        val uuid = randomUUID()
        println(uuid)
        assertNotNull(uuid)
    }

}