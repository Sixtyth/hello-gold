package com.hellogold.app.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.hellogold.app.utils.GenerateUtil
import com.hellogold.network.HelloGoldApi
import com.hellogold.network.repository.HelloGoldRepository
import com.hellogold.network.response.RegisterResponse
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class RegisterViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var helloGoldApi: HelloGoldApi

    @Mock
    private lateinit var apiClient: HelloGoldRepository

    private lateinit var viewModel: RegisterViewModel

    @Mock
    var observer: Observer<RegisterViewModel.Status>? = null

    private val dummyEmail = "test1@mail.com"

    @Before
    fun setUp() {
        /* init Rx android */
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        /* init Mock */
        MockitoAnnotations.initMocks(this)
        viewModel = RegisterViewModel(apiClient)
        viewModel.status.observeForever(observer!!)
    }

    @Test
    fun testNull() {
        `when`(
            apiClient.register(
                dummyEmail,
                GenerateUtil.uuid(),
                GenerateUtil.randomString(8),
                "false",
                dummyEmail
            )
        ).thenReturn(null)
        assertNotNull(viewModel.status)
        assertTrue(viewModel.status.hasObservers())
    }

    @Test
    fun registerSuccess() {
        // Mock API response
        `when`(
            apiClient.register(
                dummyEmail,
                GenerateUtil.uuid(),
                GenerateUtil.randomString(8),
                "false",
                dummyEmail
            )
        ).thenReturn(
            Single.just(
                RegisterResponse(
                    "4eYOjrEWxBfPd8pSWzjUcXb2TK6gp4cKaDz3gxPj",
                    "4a9a94e0-d2c2-4755-985c-76adea74c068",
                    "Ob2oVcSfLKgPbnOCg5GgSNqrQSXkE5rHLgxJWcXm",
                    "45800006975"
                )
            )
        )
        viewModel.register(
            dummyEmail,
            GenerateUtil.uuid(),
            GenerateUtil.randomString(8),
            "false",
            dummyEmail
        )
        verify(observer)?.onChanged(ArgumentMatchers.any(RegisterViewModel.Status.Loading::class.java))
        verify(observer)?.onChanged(ArgumentMatchers.any(RegisterViewModel.Status.Success::class.java))
    }

    @Test
    fun registerError() {
        // Mock API response
        `when`(
            apiClient.register(
                dummyEmail,
                GenerateUtil.uuid(),
                GenerateUtil.randomString(8),
                "false",
                dummyEmail
            )
        ).thenReturn(
            Single.error(
                Throwable("Api Error")
            )
        )
        /*viewModel.register(
            dummyEmail,
            GenerateUtil.uuid(),
            GenerateUtil.randomString(8),
            "false",
            dummyEmail
        )
        verify(observer)?.onChanged(ArgumentMatchers.any(RegisterViewModel.Status.Loading::class.java))
        verify(observer)?.onChanged(ArgumentMatchers.any(RegisterViewModel.Status.Error::class.java))*/
    }
}
