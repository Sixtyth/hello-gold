package com.hellogold.app.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.hellogold.app.utils.GenerateUtil
import com.hellogold.network.HelloGoldApi
import com.hellogold.network.repository.HelloGoldRepository
import com.hellogold.network.response.RegisterResponse
import com.hellogold.network.response.SpotPriceResponse
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class GoldSportViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var helloGoldApi: HelloGoldApi

    @Mock
    lateinit var apiClient: HelloGoldRepository

    private lateinit var viewModel: GoldSportViewModel

    @Mock
    var observer: Observer<GoldSportViewModel.Status>? = null

    private val dummyEmail = "test1@mail.com"

    @Before
    fun setUp() {
        /* init Rx android */
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        /* init Mock */
        MockitoAnnotations.initMocks(this)
        viewModel = GoldSportViewModel(apiClient)
        viewModel.goldStatus.observeForever(observer!!)
    }

    @Test
    fun testNull() {
        `when`(apiClient.getGoldPrice()).thenReturn(null)
        assertNotNull(viewModel.goldStatus)
        assertTrue(viewModel.goldStatus.hasObservers())
    }

    @Test
    fun fetchDataSuccess() {
        // Mock API response
        `when`(apiClient.getGoldPrice()).thenReturn(Single.just(SpotPriceResponse(2200.00, 2200.00, 1.0, "asdfsdaf")))
        viewModel.getGoldSpotPrice()
        verify(observer)?.onChanged(ArgumentMatchers.any(GoldSportViewModel.Status.Loading::class.java))
        verify(observer)?.onChanged(ArgumentMatchers.any(GoldSportViewModel.Status.Success::class.java))
    }

    @Test
    fun fetchDataError() {
        // Mock API response
        `when`(apiClient.getGoldPrice()).thenReturn(Single.error(Throwable("Api Error")))
        viewModel.getGoldSpotPrice()
        verify(observer)?.onChanged(ArgumentMatchers.any(GoldSportViewModel.Status.Loading::class.java))
        verify(observer)?.onChanged(ArgumentMatchers.any(GoldSportViewModel.Status.Error::class.java))
    }

}