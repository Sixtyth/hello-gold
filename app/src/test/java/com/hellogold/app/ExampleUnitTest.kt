package com.hellogold.app

import org.junit.Test

import org.junit.Assert.*
import java.text.SimpleDateFormat

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun booleanToString() {
        val boolean = false
        println(boolean)
    }

    @Test
    fun stringToDateTime() {
        val sDate1 = "2019-11-10T11:28:03.794+00:00"
        val date1 = SimpleDateFormat("dd-MM-yyyy").parse(sDate1)
        println(sDate1 + "\t" + date1)

        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val output = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val d = sdf.parse(sDate1)
        val formattedTime = output.format(d!!)
        println(formattedTime)
    }
}
