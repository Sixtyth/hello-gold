package com.hellogold.app.persistence

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PreferencesTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var store : Preferences

    private val token = "4eYOjrEWxBfPd8pSWzjUcXb2TK6gp4cKaDz3gxPj"
    private val publicKey = "4a9a94e0-d2c2-4755-985c-76adea74c068"
    private val apiKey = "Ob2oVcSfLKgPbnOCg5GgSNqrQSXkE5rHLgxJWcXm"
    private val accountNumber = "45800006975"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        store.storeApiToken(token)
        store.storePublicKey(publicKey)
        store.storeApiToken(apiKey)
        store.storeAccountNumber(accountNumber)
    }

    @Test
    fun getToken() {
        Mockito.`when`(store.getToken()).thenReturn(token)
        assertNotNull(store.getToken())
    }

    @Test
    fun getPublicKey() {
        Mockito.`when`(store.getPublicKey()).thenReturn(publicKey)
        assertNotNull(store.getPublicKey())
    }

    @Test
    fun getApiKey() {
        Mockito.`when`(store.getApiKey()).thenReturn(apiKey)
        assertNotNull(store.getApiKey())
    }

    @Test
    fun getAccountNumber() {
        Mockito.`when`(store.getAccountNumber()).thenReturn(accountNumber)
        assertNotNull(store.getAccountNumber())
    }
}