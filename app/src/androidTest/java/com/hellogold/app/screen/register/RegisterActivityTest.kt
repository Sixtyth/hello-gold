package com.hellogold.app.screen.register

import android.view.View
import android.widget.EditText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.google.android.material.textfield.TextInputLayout
import com.hellogold.app.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RegisterActivityTest {

    @get:Rule
    var activityRule: ActivityTestRule<RegisterActivity> =
        ActivityTestRule(RegisterActivity::class.java)

    private val email = "test@test.test"
    private val password = "123456789"

    @Before
    fun setUp() {

    }

    @Test
    fun emailIsNotEmpty() {
        val textInput = activityRule.activity.findViewById(R.id.register_text_input_email) as TextInputLayout
        textInput.editText?.setText(email)
        Assert.assertNotNull(email, textInput.editText?.text.toString())
    }

    private fun withError(expected: String): Matcher<View> {
        return object : TypeSafeMatcher<View>() {
            override fun matchesSafely(item: View): Boolean {
                return if (item is EditText) {
                    item.error.toString() == expected
                } else false
            }

            override fun describeTo(description: Description) {
                description.appendText("Not found error message$expected, find it!")
            }
        }
    }
}